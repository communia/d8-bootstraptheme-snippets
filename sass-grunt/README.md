Per poder crear un sass bootstrap subtheme a partir de l'starterkit de drupal bootstrap.
A dins del subtema:
- forma auto:
    - `wget https://gitlab.com/communia/d8-bootstraptheme-snippets/raw/master/sass-grunt/package.json` 
    - `npm install`
    - `ln -s .node_modules/bootstrap-sass bootstrap`

- forma manual:

```wget https://github.com/twbs/bootstrap-sass/archive/master.zip  
unzip master.zip
mv bootstrap-sass-master bootstrap  
npm install grunt-cli --save-dev
npm install grunt-contrib-watch --save-dev  
npm install grunt-contrib-jshint --save-dev
npm install grunt-contrib-uglify --save-dev  
npm install grunt-contrib-concat --save-dev   
npm install grunt-contrib-sass --save-dev   
npm install grunt-contrib-compass --save-dev  
```

- `cat bootstrap/assets/stylesheets/bootstrap/_variables.scss >> scss/_default-variables.scss`

- Si volem afegir un estil complementari: `echo -e "\n@import 'myotherscss';"  >>  scss/style.scss` , `touch scss/myotherscss.scss` .

- `wget https://gitlab.com/communia/d8-bootstraptheme-snippets/raw/master/sass-grunt/DrupalBootstrap-Gruntfile.js -O Gruntfile.js`

- Executar:
```
mkdir css
screen:
  1> node_modules/.bin/grunt watch
  2> echo "" >> scss/style.scss # per fer trigger de la compilació.
```


- A partir d'aqui seguir guia de :  
http://drupal-bootstrap.org/api/bootstrap/starterkits%21sass%21README.md/group/subtheme_sass/7#setup

