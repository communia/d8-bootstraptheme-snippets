jQuery.each(['show', 'hide'], function (i, ev) {
            var el = jQuery.fn[ev];
            jQuery.fn[ev] = function () {
              this.trigger(ev);
              return el.apply(this, arguments);
            };
});

jQuery(document).on("scroll", function() {

        if(jQuery(document).scrollTop()>100) {
                jQuery("header#navbar").removeClass("large").addClass("small");

        } else {
                jQuery("header#navbar").removeClass("small").addClass("large");
        }

});

